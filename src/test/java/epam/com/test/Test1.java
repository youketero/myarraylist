package epam.com.test;
import com.epam.task1.MyArrayList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Test1 {
    private MyArrayList arr;

    @Before
    public void createObj() {
        arr = new MyArrayList(4);
        int num1 = 12;
        int num2 = 14;
        int num3 = 16;
        String str1 = "String1";
        String str2 = "String2";
        String str3 = "String4";
        String str4 = "String4";
        String str5 = "String5";
        arr.add(num1);
        arr.add(num2);
        arr.add(num3);
        arr.add(str1);
        arr.add(str2);
        arr.add(str3);
        arr.add(str4);
        arr.add(str5);
        assertNotNull(arr);
    }

    @Test
    public void checkSize() {
        Assert.assertEquals(8, arr.size());
        System.out.println("Actual result is " + arr.size() + ". Expected result is 8");
    }

    @Test
    public void checkAdd() {
        String str5 = "String5";
        Assert.assertEquals("String5", arr.add(str5));
        System.out.println("Actual result is " + str5 + " . Expected result is String5 ");
    }

    @Test
    public void checkClear() {
        arr.clear();
        Assert.assertEquals(0, arr.size());
        System.out.println("Actual result is " + arr.size() + ". Expected result is 0");
    }

    @Test
    public void checkContains() {
        Assert.assertEquals(true, arr.contains(12));
        System.out.println("Actual result is " + arr.contains(12) + ". Expected result is true");
    }

    @Test
    public void checkRemoveByIndex() {
        Assert.assertEquals(16, arr.remove(2));
        System.out.println("Actual result is " + arr.remove(2) + ". Expected result is 16");
    }

    @Test
    public void checkRemoveByValue() {
        Assert.assertEquals("String1", arr.remove("String1"));
        System.out.println("Actual result is " + arr.remove("String1") + ". Expected result is String1");
    }
}

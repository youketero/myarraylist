package com.epam.task2;

import java.util.HashMap;
import java.util.Map;

public class Database {
    private Map<Integer, User> userMap;
    private int id = 1;
    private int capacity;

    public Database(int capacity) {
        this.capacity = capacity;
        this.userMap = new HashMap<Integer, User>(capacity);
    }

    public Map<Integer, User> save(User arg) {
        this.userMap.put(id++, arg);
        return userMap;
    }

    public void clear() {
        this.userMap = new HashMap<Integer, User>(capacity);
    }

    public void printAllUsers() {
        if(userMap.size()==0){
            System.out.println("Database is empty");
        }
        for (int i = 1; i < userMap.size() + 1; i++) {
            System.out.println("Index is " + i + " " + userMap.get(i));
        }
    }

    public boolean contains(Integer arg) {
        return userMap.containsKey(arg);
    }

    public User remove(Integer i) {
        if (i > id) {
            System.out.println("Invalid index please enter another");
        }
        return userMap.remove(i);
    }

    public Map<Integer, User> getUserMap() {
        return userMap;
    }

    public User getValue(Integer id) {
        return userMap.get(id);
    }

    public Integer getSize() {
        return userMap.size();
    }
}




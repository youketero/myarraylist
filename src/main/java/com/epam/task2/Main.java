package com.epam.task2;


public class Main {
    public static void main(String[] args) {
        Database arr = new Database(6);
        System.out.println("Contains method with empty array");
        System.out.println(arr.contains(12));
        System.out.println("-------------------------------");
        System.out.println("Print empty database ");
        arr.printAllUsers();
        System.out.println("-------------------------------");
        User userOne = new User("Andrew", "Andrew@gmail.com");
        User userSecond = new User("Jane", "Jane@gmail.com");
        User userThird = new User("Nick", "Nick@gmail.com");
        User userFouth = new User("Sam", "Sam@gmail.com");
        User userFifth = new User("Alex", "Alex@gmail.com");
        arr.save(userOne);
        arr.save(userSecond);
        arr.save(userThird);
        arr.save(userFouth);
        arr.save(userFifth);
        System.out.println("Print all values ");
        arr.printAllUsers();
        System.out.println("-------------------------------");
        System.out.println("Print remaining values");
        arr.remove(2);
        arr.printAllUsers();
        System.out.println("-------------------------------");
        System.out.println("Test method -- contains with index 1");
        System.out.println(arr.contains(1));
        System.out.println("-------------------------------");
        arr.clear();
        System.out.println("Test method clear");
        arr.printAllUsers();

    }
}



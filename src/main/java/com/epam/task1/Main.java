package com.epam.task1;


public class Main {

    public static void main(String[] args) {
        MyArrayList arr = new MyArrayList(4);
        System.out.println("Contains method with empty array");
        System.out.println(arr.contains(12));
        System.out.println("-------------------------------");
        int num1 = 12;
        int num2 = 14;
        int num3 = 16;
        String str1 = "String1";
        String str2 = "String2";
        String str3 = "String3";
        arr.add(num1);
        arr.add(num2);
        arr.add(num3);
        arr.add(str1);
        arr.add(str2);
        arr.add(str3);
        System.out.println("Print all values ");
        arr.print();
        System.out.println("----------------------");
        System.out.println("Test method -- contains value 12");
        System.out.println("Returned " + arr.contains(12));
        System.out.println("----------------------");
        System.out.println("Test method -- size is 6 ");
        System.out.println("Actual size is " + arr.size());
        System.out.println("----------------------");
        System.out.println("Test method -- remove by index  1 is 16" );
        System.out.println("Actual result " + arr.remove(1));
        System.out.println("----------------------");
        System.out.println("Test method -- remove by value  " + str1);
        System.out.println("Actual result " + arr.remove(str1));
        System.out.println("----------------------");
        System.out.println("Print remaining values");
        arr.print();
        System.out.println("----------------------");
        arr.clear();
        arr.print();


    }

}

package epam.com.test;
import com.epam.task2.Database;
import com.epam.task2.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertNotNull;

public class Test2 {
    private Database arr;

    @Before
    public void createObj () {
        arr = new Database(12);
        User userOne = new User("Andrew", "Andrew@gmail.com");
        User userSecond = new User("Jane", "Jane@gmail.com");
        User userThird = new User("Nick", "Nick@gmail.com");
        User userFouth = new User("Sam", "Sam@gmail.com");
        User userFifth = new User("Alex", "Alex@gmail.com");
        arr.save(userOne);
        arr.save(userSecond);
        arr.save(userThird);
        arr.save(userFouth);
        arr.save(userFifth);
        assertNotNull(arr);
    }

    @Test
    public void checkSave(){
        User userOne = new User("Andrew", "Andrew@gmail.com");
        Assert.assertEquals(userOne.getName(),arr.getValue(1).getName());
        Assert.assertEquals(userOne.getEmail(),arr.getValue(1).getEmail());

    }

    @Test
    public void checkContains(){
        Assert.assertEquals(true,arr.contains(1));
    }

    @Test
    public void checkClear(){
        arr.clear();
        Assert.assertEquals(0, arr.getSize(),0);
    }

    @Test
    public void checkRemove(){
        arr.remove(1);
        Assert.assertEquals(null, arr.getValue(1));
    }


}

package com.epam.task1;

import java.util.Arrays;

public class MyArrayList<T> {
    private int lengthArray;
    private int counter;
    private T[] array;

    public MyArrayList(int lengthArray) {
        this.lengthArray = lengthArray;
        array = (T[]) (new Object[lengthArray]);
    }

    public T add(T arg) {
        if (counter == lengthArray - 1) {
            extendedArray();
        }
        array[counter++] = arg;
        return arg;
    }

    public T remove(int i) {
        if (i > counter) {
            System.out.println("Invalid index");
        } else {
            T removeValue = array[i];
            T[] array1 = Arrays.copyOfRange(array, i, counter);
            for (T t : array1) {
                array[i - 1] = t;
                i++;
            }
            this.counter--;
            array[counter] = null;
            return removeValue;
        }
        return array[0];

    }

    public T remove(T i) {
        if (getIndex(i) == -1) {
            System.out.println("Not found by value");
        } else {
            int y = getIndex(i) + 1;
            T[] array1 = Arrays.copyOfRange(array, y, counter);
            for (T t : array1) {
                array[y - 1] = t;
                y++;
            }
            this.counter--;
            array[counter] = null;
        }
        return i;

    }

    public void clear() {
        array = (T[]) (new Object[lengthArray]);
        this.counter = 0;
    }

    public int size() {
        return counter;
    }

    public void print() {
        if (counter == 0) {
            System.out.println("Nothing to print");
        }
        for (int i = 0; i < counter; i++) {
            System.out.println("Object with index " + (i + 1) + " is " + array[i]);
        }
    }

    private void extendedArray() {
        lengthArray = (int) (lengthArray * 1.5);
        T[] arr = array;
        array = (T[]) (new Object[lengthArray]);
        System.arraycopy(arr, 0, array, 0, arr.length);
    }

    public boolean contains(T arg) {
        try {
            for (int i = 0; i <= counter + 1; i++) {
                if (array[i].equals(arg)) {
                    return true;
                }
            }
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }

    private int getIndex(T arg) {
        for (int i = 0; i < counter; i++) {
            if (array[i].equals(arg)) {
                return i;
            }
        }
        return -1;
    }

}
